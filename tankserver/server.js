const express = require('express');
const app = express();
const http = require('http');
const { toUnicode } = require('punycode');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, {
  cors: {
    origins: 'kenchan605.com',
    //(url domain..)
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true
  }
});

// app.get('/', (req, res) => {
//   res.sendFile('index.html');
// });
app.use(express.static('../tankproject/build/'))

let users = {}
let bullets = [];

setInterval(() => {
  const arrayUserList = (Object.keys(users).map(key => users[key]).filter(x => !!x))
  io.emit("users", arrayUserList);
  // console.log(arrayUserList);

  if (bullets.length > 0) {
    io.emit("bullets", bullets);
    bullets = []
  }
}, 20);

io.on('connection', (socket) => {
  console.log('a user connected');

  socket.on('disconnecting', () => {
    delete users[socket.id];
    console.log('a user disconnected');
  });

  socket.on('receiveBullet', (value) => {
    // console.log(value);
    bullets.push({

      ownerId: socket.id,
      x: value.x,
      y: value.y,
      b: value.b,
      destX: value.destX,
      destY: value.destY,
    })
  })
  socket.on('updatePosition', (value) => {
    users[socket.id] = {
      id: socket.id,
      x: value.x,
      y: value.y,
      r: value.r,
      t: value.t,
      l: value.l,
      show: true,

      // hp

    }
  })
});



// {
// "123123":{x,y}
// "12313":{x,y}

// }
// [{},{},{}]
// Object.keys(users).map(x=> users[x])
// 
// ["123123", "12313"]


server.listen(4000, () => {
  console.log('listening on *:4000');
});